package com.itscap.core.interactors

import com.itscap.core.data.NotesRepository
import com.itscap.core.domain.Note

class UpdateNote(private val noteRepository: NotesRepository) {
    suspend operator fun invoke(note: Note) = noteRepository.update(note)
}