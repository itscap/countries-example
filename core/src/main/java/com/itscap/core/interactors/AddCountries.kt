package com.itscap.core.interactors

import com.itscap.core.data.CountriesRepository
import com.itscap.core.domain.Country

class AddCountries(private val countriesRepository: CountriesRepository) {
    suspend operator fun invoke(country: Country):Long = countriesRepository.add(country)
}