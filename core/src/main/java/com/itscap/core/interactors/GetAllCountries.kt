package com.itscap.core.interactors

import com.itscap.core.data.CountriesRepository

class GetAllCountries(private val countriesRepository: CountriesRepository) {
    suspend operator fun invoke() = countriesRepository.getAll()
}