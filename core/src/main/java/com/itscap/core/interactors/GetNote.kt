package com.itscap.core.interactors

import com.itscap.core.data.NotesRepository

class GetNote(private val noteRepository: NotesRepository) {
    suspend operator fun invoke(id:Long) = noteRepository.get(id)
}