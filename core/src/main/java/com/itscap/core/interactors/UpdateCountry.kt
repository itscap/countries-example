package com.itscap.core.interactors

import com.itscap.core.data.CountriesRepository

class UpdateCountry(private val countriesRepository: CountriesRepository) {
    suspend operator fun invoke(id: Long, flagBase64: String) {
        countriesRepository.update(id, flagBase64)
    }
}