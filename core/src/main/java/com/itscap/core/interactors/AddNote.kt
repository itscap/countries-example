package com.itscap.core.interactors

import com.itscap.core.data.NotesRepository
import com.itscap.core.domain.Note

class AddNote(private val noteRepository: NotesRepository) {
    suspend operator fun invoke(note: Note): Long = noteRepository.add(note)
}