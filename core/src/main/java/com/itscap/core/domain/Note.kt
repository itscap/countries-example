package com.itscap.core.domain

data class Note(
    var text: String
) {
    constructor(noteId: Long, text: String) : this(text) {
        this.noteId = noteId
    }

    var noteId: Long? = null
}
