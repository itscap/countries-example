package com.itscap.core.domain

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Country(
    var id: Long? = null,
    var name: String? = null,
    @SerializedName("name_official")
    var nameOfficial: String? = null,
    @SerializedName("code3l")
    var code: String? = null,
    var latitude: Double? = null,
    var longitude: Double? = null,
    var distanceFromBase: Float? = null,
    @SerializedName("flag")
    var flagUrl: String? = null,
    var flagBase64: String? = null,
    var noteId: Long
) : Serializable
