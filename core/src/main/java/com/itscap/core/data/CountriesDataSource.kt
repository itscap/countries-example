package com.itscap.core.data

import com.itscap.core.domain.Country

interface CountriesDataSource {

    suspend fun add(country: Country): Long

    suspend fun update(id: Long, flagBase64: String)

    suspend fun readAll(): List<Country>

}