package com.itscap.core.data

import com.itscap.core.domain.Note

interface NotesDataSource {

    suspend fun add(note: Note): Long

    suspend fun update(note: Note)

    suspend fun read(id: Long): Note

}