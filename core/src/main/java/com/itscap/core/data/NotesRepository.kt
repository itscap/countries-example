package com.itscap.core.data

import com.itscap.core.domain.Note

class NotesRepository(private val dataSource: NotesDataSource) {

    suspend fun add(note: Note): Long = dataSource.add(note)

    suspend fun get(id: Long) = dataSource.read(id)

    suspend fun update(note: Note) = dataSource.update(note)
}