package com.itscap.core.data

import com.itscap.core.domain.Country

class CountriesRepository(private val dataSource: CountriesDataSource) {

    suspend fun add(country: Country): Long = dataSource.add(country)

    suspend fun getAll() = dataSource.readAll()

    suspend fun update(id: Long, flagBase64: String) = dataSource.update(id, flagBase64)
}