package com.itscap.countriesexample

import android.util.Base64
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = "../app/src/main/AndroidManifest.xml")
class UtilsUnitTest {

    @Test
    fun stringToBase64Str() {

        val testByteArr = byteArrayOf(
            0x66, 0x6c, 0x61, 0x67, 0x20, 0x70,
            0x69, 0x63, 0x74, 0x75, 0x72, 0x65
        )
        val expected = "ZmxhZyBwaWN0dXJl\n"
        val actual = testByteArr.toBase64Str()
        Assert.assertEquals(
            "Conversion from ByteArray to Base64 String failed!",
            expected,
            actual
        )
    }

    private fun ByteArray.toBase64Str() = Base64.encodeToString(this, Base64.DEFAULT)

}