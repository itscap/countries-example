package com.itscap.countriesexample.framework

import com.itscap.core.interactors.*

data class Interactors(
    val addCountries: AddCountries,
    val getAllCountries: GetAllCountries,
    val updateCountry: UpdateCountry,
    val addNote: AddNote,
    val getNote: GetNote,
    val updateNote: UpdateNote
)