package com.itscap.countriesexample.framework

import android.app.Application
import com.itscap.core.data.CountriesRepository
import com.itscap.core.data.NotesRepository
import com.itscap.core.interactors.*
import com.itscap.countriesexample.framework.db.RoomCountriesDataSource
import com.itscap.countriesexample.framework.db.RoomNotesDataSource
import com.itscap.countriesexample.framework.networking.NetworkProvider

class CountriesApp : Application() {

    override fun onCreate() {
        super.onCreate()
        NetworkProvider.setup()

        val countriesRepo = CountriesRepository(RoomCountriesDataSource(this))
        val notesRepo = NotesRepository(RoomNotesDataSource(this))

        CountriesViewModelFactory.inject(
            this,
            Interactors(
                AddCountries(countriesRepo),
                GetAllCountries(countriesRepo),
                UpdateCountry(countriesRepo),
                AddNote(notesRepo),
                GetNote(notesRepo),
                UpdateNote(notesRepo)
            )
        )
    }
}