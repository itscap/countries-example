package com.itscap.countriesexample.framework.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class NoteEntity(
    val text: String
) {
    @PrimaryKey(autoGenerate = true)
    var noteId: Long? = null
}