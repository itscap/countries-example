package com.itscap.countriesexample.framework.networking

import com.google.gson.Gson
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import java.io.IOException

class RequestHandler {

    suspend fun <T> apiCall(
        dispatcher: CoroutineDispatcher = Dispatchers.IO,
        apiCall: suspend () -> T
    ): DataWrapper<T> {
        return withContext(dispatcher) {
            try {
                DataWrapper.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is IOException -> DataWrapper.HttpError(null, ErrorResponse().asConnection())
                    is HttpException -> DataWrapper.HttpError(
                        throwable.code(),
                        buildError(throwable)
                    )
                    else -> {
                        DataWrapper.HttpError(null, ErrorResponse().asGeneric())
                    }
                }
            }
        }
    }

    private fun buildError(throwable: HttpException): ErrorResponse? {
        return try {
            throwable.response()?.errorBody()?.string()?.let { errorRespBodyStr ->
                Gson().fromJson(errorRespBodyStr, ErrorResponse::class.java)
            }
        } catch (exception: Exception) {
            null
        }
    }

}