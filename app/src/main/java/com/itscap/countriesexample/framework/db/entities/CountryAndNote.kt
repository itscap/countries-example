package com.itscap.countriesexample.framework.db.entities

import androidx.room.Embedded
import androidx.room.Relation

data class CountryAndNote(
    @Embedded
    val countryEntity: CountryEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "noteId"
    )
    val noteEntity: NoteEntity
)
