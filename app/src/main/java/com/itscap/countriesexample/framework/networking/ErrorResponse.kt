package com.itscap.countriesexample.framework.networking

import android.content.Context
import com.itscap.countriesexample.R

class ErrorResponse() {

    companion object {

        const val ERROR_CODE_GENERIC = 1000L
        const val ERROR_CODE_FORBIDDEN = 1001L
        const val ERROR_CODE_CONNECTION = 1002L
        const val ERROR_CODE_SERVER_ISSUE = 1003L
        const val ERROR_CODE_TOKEN_EXPIRED = 1004L
        const val ERROR_CODE_INTERNAL_SERVER_ERROR = 1005
    }

    var httpErrorCode: Int = -1
    var mappedErrorCode: Long = -1 // Errors mapped in-app

    constructor(mappedErrorCode: Long) : this() {
        this.mappedErrorCode = mappedErrorCode
    }

    fun asGeneric(): ErrorResponse {
        mappedErrorCode = ERROR_CODE_GENERIC
        return this
    }

    fun asForbidden(): ErrorResponse {
        mappedErrorCode = ERROR_CODE_FORBIDDEN
        return this
    }

    fun asConnection(): ErrorResponse {
        mappedErrorCode = ERROR_CODE_CONNECTION
        return this
    }

    fun asTokenExpired(): ErrorResponse {
        mappedErrorCode = ERROR_CODE_TOKEN_EXPIRED
        return this
    }

    fun withHttpErrorCode(httpErrorCode: Int): ErrorResponse {
        this.httpErrorCode = httpErrorCode
        return this
    }

    fun getMessage(context: Context?) = getLocalString(context, mappedErrorCode)

    private fun getLocalString(context: Context?, code: Long?): String {
        var message: String? = null
        /**
         * Add errors if necessary
         */
        context?.let {
            message = when (code) {
                ERROR_CODE_GENERIC -> it.getString(R.string.string_error_generic)
                else -> it.getString(R.string.string_error_generic)
            }
        }
        return message ?: "Spiacente c'è stato un problema."
    }

}