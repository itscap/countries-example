package com.itscap.countriesexample.framework.networking

import com.itscap.core.domain.Country
import retrofit2.http.GET
import retrofit2.http.Headers

interface NetworkApi {

    @GET("countries")
    @Headers("Content-Type: application/json")
    suspend fun getCountriesList(): List<Country>

}