package com.itscap.countriesexample.framework.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CountryEntity(
    @PrimaryKey
    val id: Long,
    val name: String?,
    val nameOfficial: String?,
    val code: String?,
    var latitude: Double?,
    var longitude: Double?,
    var distanceFromBase: Float?,
    val flagUrl: String?,
    val flagBase64: String?,
    val noteId: Long
)