package com.itscap.countriesexample.framework.db.dao

import androidx.room.*
import com.itscap.countriesexample.framework.db.entities.CountryAndNote
import com.itscap.countriesexample.framework.db.entities.CountryEntity
import com.itscap.countriesexample.framework.db.entities.NoteEntity

@Dao
interface CountriesDao {

    //region COUNTRY

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addCountry(countryEntity: CountryEntity): Long

    @Query("UPDATE countryentity SET flagBase64 = :flagBase64 WHERE id = :id")
    suspend fun updateCountry(id: Long, flagBase64: String)

    @Query("SELECT * FROM countryentity ORDER BY distanceFromBase")
    suspend fun getAllCountries(): List<CountryEntity>

    //endregion

    //region NOTE

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addNote(noteEntity: NoteEntity): Long

    @Query("UPDATE noteentity SET text = :text WHERE noteId = :id")
    suspend fun updateNote(id: Long, text: String)

    @Query("SELECT * FROM noteentity WHERE noteId = :id")
    suspend fun getNote(id: Long): NoteEntity

    //endregion

    //region COUNTRY AND NOTE

    @Transaction
    @Query("SELECT * FROM CountryEntity WHERE noteId = :noteId")
    suspend fun getCountryAndNoteWithId(noteId: Long): CountryAndNote

    //endregion
}