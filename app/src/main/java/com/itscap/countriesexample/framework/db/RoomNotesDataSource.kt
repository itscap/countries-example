package com.itscap.countriesexample.framework.db

import android.content.Context
import com.itscap.core.data.NotesDataSource
import com.itscap.core.domain.Note
import com.itscap.countriesexample.framework.db.entities.NoteEntity

class RoomNotesDataSource(context: Context) : NotesDataSource {

    private val countriesDao = CountriesDatabase.invoke(context).countriesDao()

    override suspend fun add(note: Note): Long = countriesDao.addNote(NoteEntity(note.text))

    override suspend fun update(note: Note) = countriesDao.updateNote(note.noteId ?: 0, note.text)

    override suspend fun read(id: Long): Note {
        val noteEntity = countriesDao.getCountryAndNoteWithId(noteId = id).noteEntity
        return Note(noteEntity.noteId ?: 0, noteEntity.text)
    }
}