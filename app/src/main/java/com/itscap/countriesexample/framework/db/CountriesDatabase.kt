package com.itscap.countriesexample.framework.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.itscap.countriesexample.framework.db.dao.CountriesDao
import com.itscap.countriesexample.framework.db.entities.CountryEntity
import com.itscap.countriesexample.framework.db.entities.NoteEntity


const val DB_VERSION = 2
const val DB_NAME = "countries-example.db"

@Database(
    entities = [CountryEntity::class, NoteEntity::class],
    version = DB_VERSION
)
abstract class CountriesDatabase : RoomDatabase() {
    abstract fun countriesDao(): CountriesDao

    companion object {
        @Volatile
        private var instance: CountriesDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance
            ?: synchronized(LOCK) { instance ?: buildDatabase(context).also { instance = it } }

        private fun buildDatabase(context: Context) = Room
            .databaseBuilder(
                context,
                CountriesDatabase::class.java,
                DB_NAME
            )
            .fallbackToDestructiveMigration()
            .build()
    }
}
