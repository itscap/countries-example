package com.itscap.countriesexample.framework.db

import android.content.Context
import com.itscap.core.data.CountriesDataSource
import com.itscap.core.domain.Country
import com.itscap.countriesexample.framework.db.entities.CountryEntity

class RoomCountriesDataSource(context: Context) : CountriesDataSource {

    private val countriesDao = CountriesDatabase.invoke(context).countriesDao()

    override suspend fun add(country: Country): Long = countriesDao.addCountry(
        CountryEntity(
            id = country.id ?: -1,
            name = country.name,
            nameOfficial = country.nameOfficial,
            code = country.code,
            latitude = country.latitude,
            longitude = country.longitude,
            distanceFromBase = country.distanceFromBase,
            flagUrl = country.flagUrl,
            flagBase64 = null,
            noteId = country.noteId
        )
    )

    override suspend fun update(id: Long, flagBase64: String) =
        countriesDao.updateCountry(id, flagBase64)


    override suspend fun readAll(): List<Country> = countriesDao.getAllCountries().map {
        Country(
            id = it.id,
            name = it.name,
            nameOfficial = it.nameOfficial,
            code = it.code,
            latitude = it.latitude,
            longitude = it.longitude,
            distanceFromBase = it.distanceFromBase,
            flagUrl = it.flagUrl,
            flagBase64 = it.flagBase64,
            noteId = it.noteId
        )
    }

}