package com.itscap.countriesexample.framework

import android.util.Base64
import java.text.DecimalFormat

object Utils {

    fun formatDistance(distanceInMt: Float): String {
        val decimalFormat = DecimalFormat("0.00")
        return decimalFormat.format(distanceInMt / 1000)
    }

    fun ByteArray.toBase64Str() = Base64.encodeToString(this, Base64.DEFAULT)

    fun String.toByteArr() = Base64.decode(this, Base64.DEFAULT)

}