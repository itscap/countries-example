package com.itscap.countriesexample.framework.networking

sealed class DataWrapper<out T> {
    data class Success<out T>(val data: T) : DataWrapper<T>()
    data class HttpError(val code: Int? = null, val error: ErrorResponse? = null) : DataWrapper<Nothing>()
}