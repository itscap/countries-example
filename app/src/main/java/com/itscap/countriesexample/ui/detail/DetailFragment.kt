package com.itscap.countriesexample.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.itscap.core.domain.Country
import com.itscap.core.domain.Note
import com.itscap.countriesexample.R
import com.itscap.countriesexample.framework.CountriesViewModelFactory
import com.itscap.countriesexample.framework.Utils.formatDistance
import com.itscap.countriesexample.framework.Utils.toByteArr
import com.itscap.countriesexample.framework.networking.ErrorResponse
import com.itscap.countriesexample.ui.BaseFragment
import com.itscap.countriesexample.ui.customviews.Alert
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.loading_dialog_layout.*


class DetailFragment : BaseFragment() {

    companion object {
        const val COUNTRY_KEY = "COUNTRY_KEY"
    }

    private val viewModel by lazy {
        ViewModelProvider(this, CountriesViewModelFactory)
            .get(DetailViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.loadingLiveData.observe(viewLifecycleOwner) { isLoading ->
            loading_dialog.visibility = if (isLoading) View.VISIBLE else View.GONE
        }

        viewModel.countryLiveData.observe(viewLifecycleOwner) { country ->
            bindCountry(country)
        }

        et_note.addTextChangedListener {
            viewModel.noteLiveData.apply {
                val note = value?.apply { text = it.toString() }
                postValue(note)
            }
        }

        btn_save.setOnClickListener {
            viewModel.updateNote()
        }

        if (viewModel.countryLiveData.value == null) {
            retrieveData()
        } else {
            bindCountry(viewModel.countryLiveData.value)
            bindNote(viewModel.noteLiveData.value)
        }
    }

    private fun bindCountry(country: Country?) {
        country?.let {
            tv_name.text = it.name
            tv_name_ofc.text = it.nameOfficial
            tv_code.text = it.code
            tv_lat.text = it.latitude.toString()
            tv_long.text = it.longitude.toString()
            tv_distance.text = String.format(
                getString(R.string.string_distance_from_headquarter),
                formatDistance(it.distanceFromBase ?: 0f)
            )

            val flagBase64 = it.flagBase64
            Glide.with(img_flag)
                .load(if (flagBase64 != null) flagBase64.toByteArr() else it.flagUrl)
                .fallback(R.drawable.ic_launcher_background)
                .into(img_flag)
        }
    }

    private fun bindNote(note: Note?) {
        note?.let {
            et_note.setText(note.text)
        }
    }

    private fun retrieveData() {
        (arguments?.getSerializable(COUNTRY_KEY) as? Country)
            ?.let {
                viewModel.countryLiveData.postValue(it)
                viewModel.getNote(it.noteId).observe(viewLifecycleOwner) { note ->
                    bindNote(note)
                    viewModel.noteLiveData.postValue(note)
                }
            }
            ?: run {
                Alert()
                    .withDesc(ErrorResponse().getMessage(context))
                    .andButtonPositive(getString(R.string.string_close)) {
                        requireActivity().onBackPressed()
                    }
                    .show(this)
            }
    }
}