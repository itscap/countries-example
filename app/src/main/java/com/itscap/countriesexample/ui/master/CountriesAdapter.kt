package com.itscap.countriesexample.ui.master

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.itscap.core.domain.Country
import com.itscap.countriesexample.R
import com.itscap.countriesexample.framework.Utils.toByteArr

class CountriesAdapter(context: Context) :
    RecyclerView.Adapter<CountriesAdapter.CountryViewHolder>() {

    private val glide: RequestManager = Glide.with(context)
    private var countriesList: MutableList<Country> = mutableListOf()
    private var onAdapterItemClicked: (Country) -> Unit = { }
    private var onNewFlagResourceRetrieved: (Country, BitmapDrawable) -> Unit = { country, bmp ->
    }

    class CountryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val container = itemView.findViewById<ConstraintLayout>(R.id.container)
        private val imgFlag = itemView.findViewById<ImageView>(R.id.img_flag)
        private val tvTitle = itemView.findViewById<TextView>(R.id.tv_name)

        fun bind(
            glide: RequestManager, country: Country,
            onItemClicked: (Country) -> Unit,
            onNewFlagResourceRetrieved: (Country, BitmapDrawable) -> Unit
        ) {
            tvTitle.text = country.name
            loadFlagImage(glide, country, onNewFlagResourceRetrieved)

            container.setOnClickListener { onItemClicked(country) }
        }

        private fun loadFlagImage(
            glide: RequestManager,
            country: Country,
            onNewFlagResourceRetrieved: (Country, BitmapDrawable) -> Unit
        ) {

            val flagBase64 = country.flagBase64
            if (flagBase64 != null) {

                glide.load(flagBase64.toByteArr())
                    .fallback(R.drawable.ic_launcher_background)
                    .centerInside()
                    .into(imgFlag)
            } else {

                glide.load(country.flagUrl)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            resource?.let { onNewFlagResourceRetrieved(country, it as BitmapDrawable) }
                            return false
                        }

                    })
                    .fallback(R.drawable.ic_launcher_background)
                    .centerInside()
                    .into(imgFlag)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CountryViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.country_row, parent, false)
    )

    override fun getItemCount() = countriesList.size

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) =
        holder.bind(
            glide,
            countriesList[position],
            onAdapterItemClicked,
            onNewFlagResourceRetrieved
        )

    fun onItemClicked(onAdapterItemClicked: (Country) -> Unit): CountriesAdapter {
        this.onAdapterItemClicked = onAdapterItemClicked
        return this
    }

    fun onNewFlagResourceRetrieved(onNewFlagResourceRetrieved: (Country, BitmapDrawable) -> Unit): CountriesAdapter {
        this.onNewFlagResourceRetrieved = onNewFlagResourceRetrieved
        return this
    }

    fun setList(list: List<Country>) {
        countriesList.clear()
        countriesList.addAll(list)
        notifyDataSetChanged()
    }
}