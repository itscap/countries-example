package com.itscap.countriesexample.ui.master

import android.app.Application
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.itscap.core.domain.Country
import com.itscap.core.domain.Note
import com.itscap.countriesexample.framework.Interactors
import com.itscap.countriesexample.framework.Utils.toBase64Str
import com.itscap.countriesexample.framework.networking.DataWrapper
import com.itscap.countriesexample.framework.networking.DataWrapper.HttpError
import com.itscap.countriesexample.framework.networking.DataWrapper.Success
import com.itscap.countriesexample.framework.networking.NetworkProvider
import com.itscap.countriesexample.framework.networking.RequestHandler
import com.itscap.countriesexample.ui.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream


class MasterViewModel(app: Application, interactors: Interactors) :
    BaseViewModel(app, interactors) {

    companion object {
        const val BASE_LAT = 45.55449017761554
        const val BASE_LONG = 12.303616284403285
        const val BASE_LOCATION_TAG = "baseLocation"
        const val COUNTRY_LOCATION_TAG = "countryLocation"
    }

    var countriesListLiveData: MutableLiveData<List<Country>> = MutableLiveData()

    private suspend fun getCountriesListFromServer(): DataWrapper<List<Country>> =
        RequestHandler().apiCall { NetworkProvider.api.getCountriesList() }

    private fun drawableToBase64Str(bitmap: Bitmap): String? {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray().toBase64Str()
    }

    private fun getDistance(baseLocation: Location, country: Country): Float {
        val countryLocation = Location(COUNTRY_LOCATION_TAG).apply {
            latitude = country.latitude ?: 0.0
            longitude = country.longitude ?: 0.0
        }

        return baseLocation.distanceTo(countryLocation)
    }

    private suspend fun addAllCountries(countriesList: List<Country>) {

        val baseLocation = Location(BASE_LOCATION_TAG).apply {
            latitude = BASE_LAT
            longitude = BASE_LONG
        }

        countriesList.forEach { country ->
            val newNoteId = interactors.addNote(Note(""))
            country.apply {
                distanceFromBase = getDistance(baseLocation, country)
                noteId = newNoteId
            }
            interactors.addCountries(country)
        }
    }

    fun updateCountryWithFlag(country: Country, bitmapDrawable: BitmapDrawable) {
        viewModelScope.launch(Dispatchers.IO) {
            drawableToBase64Str(bitmapDrawable.bitmap)?.let { base64Str ->
                interactors.updateCountry(country.id ?: -1, base64Str)
            }
        }
    }

    fun getCountriesList(forceRefresh: Boolean): MutableLiveData<DataWrapper<List<Country>>> {
        loadingLiveData.postValue(true)
        val liveData = MutableLiveData<DataWrapper<List<Country>>>()

        viewModelScope.launch(Dispatchers.IO) {
            //Retrieve countries from DB
            val storedCountries = interactors.getAllCountries()
            if (forceRefresh || storedCountries.isNullOrEmpty()) {
                //Retrieve from server
                when (val resp = getCountriesListFromServer()) {
                    is Success -> {
                        addAllCountries(resp.data)//Insert to DB
                        getCountriesList(false)//Recurse to get data from DB
                    }
                    else -> {
                        loadingLiveData.postValue(false)
                        liveData.postValue(resp as HttpError)
                    }
                }
            } else {
                //Countries retrieved from DB, update livedata
                val dataWrapper = Success(storedCountries)
                countriesListLiveData.postValue(dataWrapper.data)
                liveData.postValue(dataWrapper)
                loadingLiveData.postValue(false)
            }
        }
        return liveData
    }

}