package com.itscap.countriesexample.ui.detail

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.itscap.core.domain.Country
import com.itscap.core.domain.Note
import com.itscap.countriesexample.framework.Interactors
import com.itscap.countriesexample.ui.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DetailViewModel(app: Application, interactors: Interactors) :
    BaseViewModel(app, interactors) {

    var countryLiveData: MutableLiveData<Country> = MutableLiveData()
    var noteLiveData: MutableLiveData<Note> = MutableLiveData()

    fun getNote(id: Long): LiveData<Note> {
        loadingLiveData.postValue(true)
        val noteLiveData = MutableLiveData<Note>()
        viewModelScope.launch(Dispatchers.IO) {
            val note = interactors.getNote(id)
            noteLiveData.postValue(note)
            loadingLiveData.postValue(false)
        }
        return noteLiveData
    }

    fun updateNote() {
        loadingLiveData.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {
            noteLiveData.value?.let {
                interactors.updateNote(it)
                loadingLiveData.postValue(false)
            }
        }
    }
}