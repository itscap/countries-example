package com.itscap.countriesexample.ui.master

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.itscap.countriesexample.R
import com.itscap.countriesexample.framework.CountriesViewModelFactory
import com.itscap.countriesexample.framework.networking.DataWrapper
import com.itscap.countriesexample.ui.BaseFragment
import com.itscap.countriesexample.ui.customviews.Alert
import com.itscap.countriesexample.ui.detail.DetailFragment.Companion.COUNTRY_KEY
import kotlinx.android.synthetic.main.fragment_master.*

class MasterFragment : BaseFragment() {

    private val viewModel by lazy {
        ViewModelProvider(this, CountriesViewModelFactory)
            .get(MasterViewModel::class.java)
    }
    var countriesAdapter: CountriesAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(R.layout.fragment_master, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        countriesAdapter = CountriesAdapter(view.context)

        viewModel.loadingLiveData.observe(viewLifecycleOwner) { isLoading ->
            sr_layout.isRefreshing = isLoading
        }

        viewModel.countriesListLiveData.observe(viewLifecycleOwner) { list ->
            countriesAdapter?.setList(list)
        }

        rv_countries.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = countriesAdapter
                ?.onItemClicked { country ->
                    findNavController().navigate(
                        R.id.action_masterFragment_to_detailFragment,
                        Bundle().apply { putSerializable(COUNTRY_KEY, country) }
                    )
                }
                ?.onNewFlagResourceRetrieved { country, bitmapDrawable ->
                    viewModel.updateCountryWithFlag(country, bitmapDrawable)
                }
        }

        sr_layout.setOnRefreshListener {
            getCountriesList(view.context, forceRefresh = true)
        }

        if (viewModel.countriesListLiveData.value == null) {
            getCountriesList(view.context)
        }
    }

    override fun showToolbarBackButton() = false

    private fun getCountriesList(context: Context, forceRefresh: Boolean = false) {
        viewModel.getCountriesList(forceRefresh).observe(viewLifecycleOwner) { dataWrapper ->

            if (dataWrapper is DataWrapper.HttpError) {
                Alert()
                    .withDesc(dataWrapper.error?.getMessage(context))
                    .andButtonPositive(getString(R.string.string_retry)) {
                        getCountriesList(context)
                    }
                    .show(this)
            }
        }
    }

}