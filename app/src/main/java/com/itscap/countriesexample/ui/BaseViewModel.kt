package com.itscap.countriesexample.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.itscap.countriesexample.framework.Interactors

open class BaseViewModel(val app: Application, val interactors: Interactors) : AndroidViewModel(app) {

    var loadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
}