package com.itscap.countriesexample

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.itscap.core.domain.Country
import com.itscap.countriesexample.ui.master.MasterFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MasterFragmentTest {

    private var masterFragment: MasterFragment? = null
    private var navController: TestNavHostController? = null

    @Before
    fun before() {
        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        GlobalScope.launch(Dispatchers.Main) {
            navController?.setGraph(R.navigation.nav_graph)
        }
        launchFragmentInContainer<MasterFragment>().onFragment { fragment ->
            Navigation.setViewNavController(fragment.requireView(), navController)
            masterFragment = fragment
        }
    }

    @Test
    fun testNavigationToDetail() {
        GlobalScope.launch(Dispatchers.Main) {
            /**
             *  Add just a quick mocked obj to list,
             *  just to allow the adapter to render a view
             *  so we can click on it
             */
            val mockCountriesList = listOf(
                Country(
                    id = 0,
                    name = null,
                    nameOfficial = null,
                    code = null,
                    latitude = null,
                    longitude = null,
                    distanceFromBase = null,
                    flagUrl = null,
                    flagBase64 = null,
                    noteId = 0
                )
            )
            masterFragment?.countriesAdapter?.setList(mockCountriesList)

            //Click viewHolder
            onView(ViewMatchers.withId(R.id.rv_countries))
                .perform(
                    RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                        0,
                        ViewActions.click()
                    )
                )

            assertEquals(navController?.currentDestination?.id, R.id.detailFragment)
        }
    }
}

/*
@Test
fun testNavigationToDetail() {

   val navController = TestNavHostController(ApplicationProvider.getApplicationContext())
   GlobalScope.launch(Dispatchers.Main) {
       navController.setGraph(R.navigation.nav_graph)
   }

   launchFragmentInContainer<MasterFragment>().onFragment { fragment ->
       Navigation.setViewNavController(fragment.requireView(), navController)

       val viewModel = ViewModelProvider(
           fragment,
           CountriesViewModelFactory
       ).get(MasterViewModel::class.java)

       viewModel.countriesListLiveData
           .observe(fragment.viewLifecycleOwner) {

               //Click viewHolder
               onView(ViewMatchers.withId(R.id.rv_countries))
                   .perform(
                       RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                           0,
                           ViewActions.click()
                       )
                   )

               assertEquals(navController.currentDestination?.id, R.id.detailFragment)
           }

       val country = Mockito.mock(Country::class.java)
       viewModel.countriesListLiveData.postValue(listOf(country))
   }
}
*/

